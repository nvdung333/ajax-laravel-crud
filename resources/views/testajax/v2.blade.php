<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

    <!-- <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
    
</head>
<body>

    <div class="container">
        <h1>Dynamic Dependant Select Box using JQuery Ajax Example</h1>

        <form method="" action="">

            @csrf

            <div class="form-group">
                <label>Select:</label>
                <select class="form-control" id="t1">
                    <option value="">---</option>
                </select>
            </div>

            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">id</th>
                        <th scope="col">string1</th>
                        <th scope="col">string2</th>
                        <th scope="col">float</th>
                    </tr>
                </thead>
                <tbody id="tbody">

                </tbody>
            </table>

            <p id="pp"></p>

            <div class="form-group">
                <button class="btn btn-success" type="submit">Submit</button>
            </div>
        </form>

    </div>

    <script>

    // // test
    // $(".table").click(function(){
    //     $("#tr").html('');
    //     $("#tr").append(
    //         '<td>0</td>'+
    //         '<td>AAA</td>'+
    //         '<td>BBB</td>'+
    //         '<td>0.01</td>')
    //     }
    // );


    var url = "{{ url('/v2') }}";

    $(document).ready(function () {
        $("#t1").ready(function () {
            $.ajax({
                url: "show2",
                type: "POST",
                data: {
                    _token:'{{ csrf_token() }}'
                },
                cache: false,
                dataType: 'json',
                success: function(dataResult) {
                    console.log(dataResult);
                    var resultData = dataResult.dataaa;
                    // console.log(resultData);
                    var bodyData = "";
                    $.each(resultData, function(index,row){
                        bodyData += "<option value="+row.id+">"+row.t1_string1+"</option>";                      
                    });
                    $("#t1").append(bodyData);
                }
            });
        });
    });


    $("#t1").change(function () {
        var t1_id = $(this).val();
        var token = $("input[name='_token']").val();
        $.ajax({
            url: "show3",
            type: 'POST',
            data: {
                t1_id: t1_id,
                _token: token
            },
            dataType: 'json',
            success: function(dataResult) {
                console.log(dataResult);
                resultData = dataResult.tadaaa;
                console.log(resultData);
                var bodyData = "";
                $("#tbody").html("");
                $.each(resultData, function(index,row){
                    bodyData +=
                    "<tr>"+
                    "<td>"+row.id+"</td>"+
                    "<td>"+row.t2_string1+"</td>"+
                    "<td>"+row.t2_string2+"</td>"+
                    "<td>"+row.t2_float+"</td>"+
                    "<tr>"
                });
                $("#tbody").append(bodyData);
            }
        })
    })



    </script>

</body>
</html>