<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <title>AJAX CRUD</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    
    <!-- <meta name="csrf-token" content="{{ csrf_token() }}"> -->
</head>
<body>
    <div class="container">
        <div>
            <br>
            <h1 style="text-align: center;">TEST AJAX LARAVEL CRUD - DŨNG_NV</h1>
            <button id="btn-add" name="btn-add" class="btn btn-primary">Create</button>
            <a class="btn btn-success" href="{{url('/')}}" role="button">Refresh</a>
        </div>

        <div style="padding: 20px 0 10px 0; font-style: italic">
            <span>***Data will be reloaded in... </span>
            <span id="countdown"></span>
            <span>second(s)</span>
        </div>

        <div class="form-group">
            <label class="form-label">Search</label>
            <input type="text" class="form-control" id="search" name="search"></input>
        </div>


        <div>
            <table class="table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>THE STRING</th>
                        <th>THE INTEGER</th>
                        <th>THE FLOAT</th>
                        <th>OPTIONS</th>
                    </tr>
                </thead>
                
                <tbody id="t3_list" name="t3_list">
                    @foreach ($therecords as $therecord)
                        <tr id="t3{{ $therecord->id }}">
                            <td>{{ $therecord->id }}</td>
                            <td>{{ $therecord->t3_string }}</td>
                            <td>{{ $therecord->t3_integer }}</td>
                            <td>{{ $therecord->t3_float }}</td>
                            <td>
                                <button class="btn btn-warning open-modal" value="{{$therecord->id}}">Edit</button>
                                <button class="btn btn-danger deleterecord" value="{{$therecord->id}}">Delete</button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            <div class="modal fade" id="AddEditModal">
                <div class="modal-dialog">
                    <div class="modal-content">

                        <div class="modal-header">
                            <h2>ADD / EDIT</h2>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div class="modal-body"> 
                            <form id="modalFormData" name="modalFormData" class="form-horizontal">
                                @csrf
                                <div class="form-group col-md-12">
                                    <label class="form-label">The String</label>
                                    <input type="text" class="form-control" id="t3_string" name="t3_string" value=""></input>
                                </div>
                                <div class="form-group col-md-12">
                                    <label class="form-label">The Integer</label>
                                    <input type="text" class="form-control" id="t3_integer" name="t3_integer" value=""></input>
                                </div>
                                <div class="form-group col-md-12">
                                    <label class="form-label">The Float</label>
                                    <input type="text" class="form-control" id="t3_float" name="t3_float" value=""></input>
                                </div>
                            </form>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" id="btn-save" value="add">Save</button>
                            <input type="hidden" id="t3_id" name="t3_id" value="0">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>

<script>
    $(document).ready(function() {
        

        // OPEN MODAL TO ADD
        $("#btn-add").click(function() {
            $("#btn-save").val("add");
            $("#modalFormData").trigger("reset");
            $("#AddEditModal").modal("show");
        });


        // OPEN MODAL TO EDIT
        $("body").on("click", ".open-modal", function () {
            var t3_id = $(this).val();
            // console.log(t3_id);
            $.get("v3/record/"+t3_id, function(data) {
                console.log(data);
                $("#t3_id").val(data.id);
                $("#t3_string").val(data.t3_string);
                $("#t3_integer").val(data.t3_integer);
                $("#t3_float").val(data.t3_float);
                $("#btn-save").val("update");
                $("#AddEditModal").modal("show");
            });
        });

        
        // CREATE OR UPDATE
        $("#btn-save").click(function (e) {
            // $.ajaxSetup({
            //     headers: {
            //         'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
            //     }
            // });
            var x = confirm("Are you sure?");
            if (x == true)
            { 
                e.preventDefault();
                var token = jQuery("input[name='_token']").val();
                var formData = {
                    t3_string: $("#t3_string").val(),
                    t3_integer: $("#t3_integer").val(),
                    t3_float: $("#t3_float").val(),
                    _token: token,
                }
                var state = $("#btn-save").val();
                var type = "POST";
                var t3_id = $("#t3_id").val();
                var ajaxurl = "{{ url('/v3/record/') }}";
                if (state == "update") {
                    var type = "PUT";
                    ajaxurl = "{{ url('/v3/record/') }}"+"/"+t3_id;
                }
                $.ajax({
                    type: type,
                    url: ajaxurl,
                    data: formData,
                    dataType: "json",
                    success: function(data) {
                        console.log(data);
                        var therecord ='<tr id="t3'+data.id+'">'+
                        '<td>'+data.id+'</td>'+
                        '<td>'+data.t3_string+'</td>'+
                        '<td>'+data.t3_integer+'</td>'+
                        '<td>'+data.t3_float+'</td>'+
                        '<td>'+'<button class="btn btn-warning open-modal" value="'+data.id+'">Edit</button> '+
                            '<button class="btn btn-danger deleterecord" value="'+data.id+'">Delete</button>'+'</td>'+
                        '</tr>'

                        if (state == "add") {
                            $("#t3_list").append(therecord);
                        } else if(state == "update") {
                            $("#t3"+t3_id).replaceWith(therecord);
                        }

                        $("#modalFormData").trigger("reset");
                        $("#AddEditModal").modal("hide");
                    },
                    error: function(data) {
                        console.log("Error:",data);
                    }
                });
            }
            else {
                console.log("Not confirm.")
            }
        });
        

        // DELETE
        $("#t3_list").on("click", ".deleterecord", function () {
            // $.ajaxSetup({
            //     headers: {
            //         'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
            //     }
            // });
            var x = confirm("Are you sure?");
            if (x == true)
            {
                var t3_id = $(this).val();
                console.log("Delete: "+t3_id);
                var token = jQuery("input[name='_token']").val();
                $.ajax({
                    type: "DELETE",
                    url: "{{ url('/v3/record/') }}"+"/"+t3_id,
                    data: {
                        _token: token,
                    },
                    success: function(data) {
                        $("#t3" + t3_id).remove();
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            }
            else {
                console.log("Not confirm.")
            }
        });


        // SEARCH
        $("#search").on('keyup', function(){
            $.ajaxSetup({
                headers: {
                    'csrftoken' : '{{ csrf_token() }}'
                }
            });
            $query = $(this).val();
            $.ajax({
                type: "GET",
                url: "{{ url('/v3/search') }}",
                data: {
                    'q': $query,
                },
                success: function(data) {
                    $("#t3_list").html(data);
                }
            });
        });


        // RELOAD
        setInterval(function() {
            console.log("reload");
            var token = jQuery("input[name='_token']").val();
            $.ajax({
                type: "POST",
                url: "{{ url('v3/reload') }}",
                data: {
                    _token: token,
                },
                dataType: 'json',
                success: function(data) {
                    var bodyData = "";
                    $("#t3_list").html("");
                    $.each(data, function(index, row) {
                        bodyData +=
                        '<tr id="t3'+row.id+'">'+
                        '<td>'+row.id+'</td>'+
                        '<td>'+row.t3_string+'</td>'+
                        '<td>'+row.t3_integer+'</td>'+
                        '<td>'+row.t3_float+'</td>'+
                        '<td>'+'<button class="btn btn-warning open-modal" value="'+row.id+'">Edit</button> '+
                            '<button class="btn btn-danger deleterecord" value="'+row.id+'">Delete</button>'+'</td>'+
                        '</tr>'
                    });
                    $("#t3_list").append(bodyData);
                }
            });
        }, 15000);

        // COUNTDOWN
        var timeleft = 15
        var countdown = setInterval(function() {
            if (timeleft <=1) {
                timeleft += 15;
            }
            timeleft -= 1;
            $("#countdown").html('');
            $("#countdown").append(timeleft);
        }, 1000);
        
        
    });
</script>
