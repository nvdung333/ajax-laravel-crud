<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

</head>
<body>

    <div class="container">
        <h1>Dynamic Dependant Select Box using JQuery Ajax Example</h1>

        <form method="" action="">
            @csrf
            <div class="form-group">
                <label>Select T1:</label>
                <select class="form-control" id="t1">
                    <option value="">---</option>
                    @foreach($the1stvalues as $the1stvalue)
                        <option value="{{ $the1stvalue->id }}">{{ $the1stvalue->t1_string1 }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label>Select T2:</label>
                <select class="form-control" id="t2">
                    <option value="">---</option>
                </select>
            </div>

            <div class="form-group">
                <button class="btn btn-success" type="submit">Submit</button>
            </div>
        </form>

    </div>

    <script>
    var url = "{{ url('/show1') }}";
    jQuery("#t1").change(function(){
        var t1_id = jQuery(this).val();
        var token = jQuery("input[name='_token']").val();

        jQuery.ajax({
            url: url,
            type: 'POST',
            data: {
                t1_id: t1_id,
                _token: token
            },
            success: function(data) {
                jQuery("#t2").html('');
                jQuery("#t2").append("<option value=''>---</option>");
                jQuery.each(data, function(key, value){
                    jQuery("#t2").append(
                        "<option value=" + value.id + ">" + value.t2_string1 + " & " + " & " + value.t2_string2 + "</option>"
                    );
                });
            }
        });
    });
</script>

</body>
</html>
