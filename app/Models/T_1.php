<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class T_1 extends Model
{
    use HasFactory;

    protected $table = 't_1';

    // protected $hidden = ['created_at', 'updated_at'];

}
