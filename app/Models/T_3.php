<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class T_3 extends Model
{
    use HasFactory;

    protected $table = 't_3';
    protected $fillable = ['t3_string', 't3_integer', 't3_float'];

    public $timestamps = false;

}
