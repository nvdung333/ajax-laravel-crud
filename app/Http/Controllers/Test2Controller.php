<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;
use App\Models\T_3;

class Test2Controller extends Controller
{
    // LOAD INDEX VIEW
    public function index() {
        $therecords = T_3::all();
        return view("testajax.crud")->with('therecords', $therecords);

        // $data = [];
        // $data['therecords'] = $therecords;
        // return view("testajax.crud", $data);
    }

    // CREATE
    public function create(Request $request) {
        $therecord = T_3::create($request->all());
        return Response::json($therecord);
    }

    // GET ID TO EDIT
    public function edit($t3_id) {
        $therecord = T_3::find($t3_id);
        return Response::json($therecord);
    }

    // UPDATE
    public function update(Request $request, $t3_id) {
        $therecord = T_3::findOrFail($t3_id);
        $therecord->t3_string = $request->t3_string;
        $therecord->t3_integer = $request->t3_integer;
        $therecord->t3_float = $request->t3_float;
        $therecord->save();
        return Response::json($therecord);
    }

    // DELETE
    public function delete($t3_id) {
        $therecord = T_3::destroy($t3_id);
        return response()->json($therecord);
    }

    // SEARCH
    public function search(Request $request) {
        if($request->ajax()) {
            $output = '';
            $therecords = T_3::where('t3_string', 'LIKE', "%".$request->q."%")->get();
            if($therecords) {
                foreach ($therecords as $key => $therecord) {
                    $output .= "<tr id='t3".$therecord->id."'>".
                    "<td>". $therecord->id . "</td>".
                    "<td>". $therecord->t3_string . "</td>".
                    "<td>". $therecord->t3_integer . "</td>".
                    "<td>". $therecord->t3_float . "</td>".
                    "<td>".
                            '<button class="btn btn-warning open-modal" value="' . $therecord->id . '">Edit</button> '.
                            '<button class="btn btn-danger deleterecord" value="' . $therecord->id . '">Delete</button>'.
                    "</td>".
                    "</tr>";
                }
            }
            return Response($output);
        }
    }

    // RELOAD
    public function reload(Request $request) {
        if($request->ajax()) {
            $therecords = T_3::all();
            return response()->json($therecords);
        }
    }
}
