<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\T_1;
use App\Models\T_2;

class Test1Controller extends Controller
{
    //
    public function v1(Request $request) {

        $the1stvalues = T_1::all();
        
        return view("testajax.v1", compact('the1stvalues'));

        // $data=[];
        // $data["the1stvalues"] = $the1stvalues;
        // return view ("testajax.v1", $data);
    }

    public function show1(Request $request) {

        if($request->ajax()) {
            $the2ndvalues = DB::table('t_2')
            ->where('t1_id',$request->t1_id)->select('id', 't2_string1', 't2_string2')
            ->get();

            return response()->json($the2ndvalues);
        }
    }

    //
    public function v2() {

        return view("testajax.v2",);
    }

    public function show2() {
        
        $thevalues = T_1::select('id', 't1_string1', 't1_string2', 't1_float')->get();

        // return response()->json(array('dataaa'=>$thevalues));
        return json_encode(array('dataaa'=>$thevalues));
    }

    public function show3(Request $request) {
        
        if($request->ajax()) {
            $thevalues = T_2::select('id', 't2_string1', 't2_string2', 't2_float')
            ->where('t1_id',$request->t1_id)->get();

            return json_encode(array('tadaaa'=>$thevalues));
        }
    }

    
}
