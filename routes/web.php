<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    // return view('welcome');
    return redirect('v3');
});


Route::get('v1', "App\Http\Controllers\Test1Controller@v1")->name('v1');
Route::post('show1', "App\Http\Controllers\Test1Controller@show1")->name('show1');

Route::get('v2', "App\Http\Controllers\Test1Controller@v2")->name('v2');
Route::post('show2', "App\Http\Controllers\Test1Controller@show2")->name('show2');
Route::post('show3', "App\Http\Controllers\Test1Controller@show3")->name('show3');


// AJAX CRUD
Route::get('v3', "App\Http\Controllers\Test2Controller@index");
Route::post('v3/record', "App\Http\Controllers\Test2Controller@create");
Route::get('v3/record/{t3_id?}', "App\Http\Controllers\Test2Controller@edit");
Route::put('v3/record/{t3_id?}', "App\Http\Controllers\Test2Controller@update");
Route::delete('v3/record/{t3_id?}', "App\Http\Controllers\Test2Controller@delete");
Route::get('v3/search', 'App\Http\Controllers\Test2Controller@search');
Route::post('v3/reload', 'App\Http\Controllers\Test2Controller@reload');
