<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateT2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_2', function (Blueprint $table) {
            $table->id();
            $table->string('t2_string1');
            $table->string('t2_string2');
            $table->float('t2_float', 12, 2);
            $table->foreignid('t1_id')->nullable();
            $table->timestamps();
            $table->foreign('t1_id')->references('id')->on('t_1');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_2');
    }
}
